# Overview

This project's primary goal is to be a simple container that can be used in multiple configurations
to demonstrate complex Kubernetes deployments. Seconary goals are to include some basic debugging
tools and/or methods for rudamentary use.

## Use cases

1. Deploy this application with an arbitrary set of ENV variables that are echoed back to the caller. The demonstration is
to simulate a configuration release where "v1" is a single deployed container, "v2" can be a second.

## Development

This application is going to focus on using Actix Web on Rust, which is deployed as a Container.
