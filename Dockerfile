FROM rust:latest as build

RUN rustup target add wasm32-unknown-unknown
RUN cargo install trunk wasm-bindgen-cli

WORKDIR /app
COPY . .

RUN cd frontend && trunk build --release
RUN cargo build --release

FROM gcr.io/distroless/cc-debian10

COPY --from=build /app/target/release/backend /app/backend
COPY --from=build /app/frontend/dist /app/dist

WORKDIR /app
CMD ["/app/backend"]