#[warn(dead_code)]
struct User {
    id: String,
    first_name: String,
    last_name: String,
}