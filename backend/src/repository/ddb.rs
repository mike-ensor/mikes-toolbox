use common::model::task::{Task, TaskState};

pub struct DDBRepository {
    task: Task,
    task_state: TaskState,
}
