mod api;
mod repository;

use api::task::{
    get_task,
};
use actix_web::{HttpServer, App, web::scope, middleware::Logger};
use actix_web_lab::web::spa;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "debug");
    std::env::set_var("RUST_BACKTRACE", "1");
    env_logger::init();

    // let config = aws_config::load_from_env().await;
    HttpServer::new(move || {
        let logger = Logger::default();

        App::new()
            .wrap(logger)
            .service(
                scope("/api")
                    .service(get_task)
            )
            .service(
                spa()
                .index_file("./dist/index.html")
                .static_resources_mount("/")
                .static_resources_location("./dist")
                .finish()
            )

    })
    .bind(("0.0.0.0", 80))?
    .run()
    .await
}
